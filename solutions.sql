CREATE DATABASE blog_db;
-- USE blog_db;

CREATE TABLE users (
id INT NOT NULL AUTO_INCREMENT,
email VARCHAR(100) NOT NULL,
password VARCHAR(300) NOT NULL,
datetime_created DATETIME NOT NULL,
PRIMARY KEY (id)
);

CREATE TABLE posts (
id INT NOT NULL AUTO_INCREMENT,
title VARCHAR(500) NOT NULL,
content VARCHAR(5000) NOT NULL,
datetime_posted DATETIME NOT NULL,
PRIMARY KEY (id),
author_id INT NOT NULL,
CONSTRAINT fk_posts_author_id
FOREIGN KEY (author_id) REFERENCES users(id)
ON UPDATE CASCADE
ON DELETE RESTRICT
);

CREATE TABLE post_likes (
id INT NOT NULL AUTO_INCREMENT,
datetime_liked DATETIME NOT NULL,
PRIMARY KEY (id),
post_id INT NOT NULL,
CONSTRAINT fk_post_likes_post_id
FOREIGN KEY (post_id) REFERENCES posts(id)
ON UPDATE CASCADE
ON DELETE RESTRICT,
user_id INT NOT NULL,
CONSTRAINT fk_post_likes_user_id
FOREIGN KEY (user_id) REFERENCES users(id)
ON UPDATE CASCADE
ON DELETE RESTRICT
);

CREATE TABLE post_comments (
id INT NOT NULL AUTO_INCREMENT,
content VARCHAR(5000),
datetime_commented DATETIME NOT NULL,
PRIMARY KEY (id),
post_id INT NOT NULL,
CONSTRAINT fk_post_comments_post_id
FOREIGN KEY (post_id) REFERENCES posts(id)
ON UPDATE CASCADE
ON DELETE RESTRICT,
user_id INT NOT NULL,
CONSTRAINT fk_post_comments_user_id
FOREIGN KEY (user_id) REFERENCES users(id)
ON UPDATE CASCADE
ON DELETE RESTRICT
);

INSERT INTO users (email,password,datetime_created) VALUES ("johnsmith@gmail.com","passwordA","2021-01-01 01:00:00"),
	("juandelacruz@gmail.com","passwordB","2021-01-01 02:00:00"),
	("janesmith@gmail.com","passwordC","2021-01-01 03:00:00"),
	("mariadelacruz@gmail.com","passwordD","2021-01-01 04:00:00"),
	("johndoe@gmail.com","passwordE","2021-01-01 05:00:00");

INSERT INTO posts (title,content,datetime_posted,author_id) VALUES ("First Code", "Hello World!","2021-01-02 01:00:00", 1),
	("Second Code", "Hello Earth!","2021-01-02 02:00:00", 1),
	("Third Code", "Welcome to Mars!","2021-01-02 03:00:00", 2),
	("Fourth Code", "Bye bye solar system!","2021-01-02 04:00:00", 4);

SELECT * FROM posts WHERE author_id = 1;

SELECT * FROM users;

UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 2;

DELETE FROM users WHERE email = "johndoe@gmail.com";